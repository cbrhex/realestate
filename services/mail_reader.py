import dependency_injector.containers as containers
import dependency_injector.providers as providers

from config.mail import MailConfig
from mail.credentials import Credentials
from mail.mail_reader import MailReader


class MailReaderService(containers.DeclarativeContainer):
    """ set credentials for read emails """
    credentials = Credentials()
    credentials.host = MailConfig.Incoming.host
    credentials.port = MailConfig.Incoming.port
    credentials.login = MailConfig.Incoming.login
    credentials.password = MailConfig.Incoming.password

    """ add mail reader to container """
    reader = providers.Factory(MailReader, credentials)
