import dependency_injector.containers as containers
import dependency_injector.providers as providers

from file_system.drivers.google_drive import GoogleDrive
from file_system.file_worker import FileWorker


class FileWorkerService(containers.DeclarativeContainer):
    driver = GoogleDrive()
    file = providers.Factory(FileWorker, driver)
