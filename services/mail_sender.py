import dependency_injector.containers as containers
import dependency_injector.providers as providers

from config.mail import MailConfig
from mail.credentials import Credentials
from mail.mail_sender import MailSender


class MailSenderService(containers.DeclarativeContainer):
    """ set credentials for send emails """
    credentials = Credentials()
    credentials.host = MailConfig.Outgoing.host
    credentials.port = MailConfig.Outgoing.port
    credentials.login = MailConfig.Outgoing.login
    credentials.password = MailConfig.Outgoing.password
    credentials.email_from = MailConfig.Outgoing.email_from

    """ add mail sender to container """
    sender = providers.Factory(MailSender, credentials)
