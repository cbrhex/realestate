## build container
`docker build -t realestate .`

## create docker volume `realestate_data` if not exists
- `docker volume create realestate_data`

## run container

#### with log
```
docker run -v realestate_volume:/home/source/session --name=realestate -it realestate
```

#### daemon
```
docker run --restart always -v realestate_volume:/home/source/session -d --name=realestate realestate
```