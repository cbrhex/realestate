FROM python:latest

# Install basics
RUN apt update
RUN apt install -y nano

ADD ./ home/source
RUN pip install -r /home/source/requirements.txt

WORKDIR /home/source
CMD ["python", "-u", "./mailer.py", "--noauth_local_webserver"]