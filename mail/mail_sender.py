import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from mail.credentials import Credentials
from mail.mails.abstract_mail import AbstractMail


class MailSender:
    def __init__(self, credentials: Credentials):
        self.host = credentials.host
        self.port = credentials.port
        self.login = credentials.login
        self.password = credentials.password
        self.email_from = credentials.email_from

    def send(self, mail: AbstractMail):
        msg = MIMEMultipart()
        msg['From'] = self.email_from
        msg['To'] = mail.get_to()
        msg['Date'] = mail.get_date()
        msg['Subject'] = mail.get_subject()
        msg.attach(MIMEText(mail.get_body(), mail.get_mime()))

        if mail.has_attachment():
            for file in mail.get_attachments():
                part = MIMEApplication(
                    file.stream,
                    Name=file.name
                )

                part['Content-Disposition'] = 'attachment; filename="%s"' % file.name
                msg.attach(part)

        server = smtplib.SMTP(self.host, self.port)
        server.ehlo()
        server.starttls()
        server.login(self.login, self.password)

        try:
            server.sendmail(self.email_from, [mail.get_to()], msg.as_string())
        except:
            print('error sending mail')

        server.quit()
