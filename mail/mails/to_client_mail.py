import env
from file_system.local import LocalFile
from mail.mails.abstract_mail import AbstractMail
from parsing.replacer import Replacer


class ToClientMail(AbstractMail):
    def __init__(self, email: str, address: str, first_name: str, last_name: str, obj_id: str):
        super().__init__()

        self.email = email
        self.address = address
        self.first_name = first_name
        self.last_name = last_name
        self.obj_id = obj_id

        self.clear_attachments()

    @staticmethod
    def get_mime() -> str:
        return 'html'

    def get_to(self) -> str:
        return env.get('EMAILS', 'TEST_EMAIL') if env.get('MISC', 'DEBUG') else self.email

    def get_subject(self) -> str:
        return "Thank you for your interest in Object #%s" % self.obj_id

    def get_body(self) -> str:
        template = LocalFile().red_file('mail/templates/to_client.html')

        return Replacer().replase(template, {
            'address': self.address,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'obj_id': self.obj_id,
        })
