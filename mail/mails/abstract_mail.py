from abc import ABC, abstractmethod
from datetime import datetime
from email.utils import formatdate

from file_system.models.file import File


class AbstractMail(ABC):
    attachments = []

    def __init__(self):
        super().__init__()

    @abstractmethod
    def get_to(self) -> str:
        pass

    @abstractmethod
    def get_subject(self) -> str:
        pass

    @abstractmethod
    def get_body(self) -> str:
        pass

    @staticmethod
    def get_date() -> datetime:
        return formatdate(localtime=True)

    @staticmethod
    def get_mime() -> str:
        return 'text'

    def get_attachments(self) -> [File]:
        return self.attachments

    def add_attachment(self, attachment: File):
        self.attachments.append(attachment)

    def has_attachment(self) -> bool:
        attachments = self.get_attachments()

        return len(attachments) > 0

    def clear_attachments(self):
        self.attachments = []
