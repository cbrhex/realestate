import env
from mail.mails.abstract_mail import AbstractMail


class TestMail(AbstractMail):
    def get_to(self) -> str:
        return env.get("EMAILS", "TEST_EMAIL")

    def get_subject(self) -> str:
        return "Test subject"

    def get_body(self) -> str:
        return "Test body"
