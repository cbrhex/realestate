import env
from file_system.local import LocalFile
from mail.mails.abstract_mail import AbstractMail
from parsing.replacer import Replacer


class ToTaskMail(AbstractMail):
    def __init__(self, email: str, address: str, first_name: str, last_name: str, obj_id: str, url: str):
        super().__init__()

        self.email = email
        self.address = address
        self.first_name = first_name
        self.last_name = last_name
        self.obj_id = obj_id
        self.url = url

        self.clear_attachments()

    @staticmethod
    def get_mime() -> str:
        return 'html'

    def get_to(self) -> str:
        return env.get("EMAILS", "TO_TASK")

    def get_subject(self) -> str:
        return "Request #%s" % self.obj_id

    def get_body(self) -> str:
        template = LocalFile().red_file('mail/templates/to_task.html')

        return Replacer().replase(template, {
            'email': self.email,
            'address': self.address,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'obj_id': self.obj_id,
            'expose_url': self.url
        })
