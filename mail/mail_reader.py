import imaplib
import email

from datetime import datetime
from mail.models.attachment import Attachment
from mail.models.letter import Letter


class MailReader:
    __email_specification = '(RFC822)'
    __mail = None

    def __init__(self, credentials):
        self.credentials = credentials

        self.__mail_login()

    def __mail_login(self):
        """Login to gmail imap account"""

        if self.__mail is None:
            self.__mail = imaplib.IMAP4_SSL(self.credentials.host)
            self.__mail.login(self.credentials.login, self.credentials.password)

    def all_by_date(self, date_from, date_to=None) -> [Letter]:
        """Get all mails by date criteria

            Args:
                date_from (:obj:`datetime`): since date.
                date_to (:obj:`datetime`, optional): before date.

            Returns:
                array: List of letters.
        """
        letters = []

        self.__mail.select("inbox")

        if date_to is not None:
            type, data = self.__mail.search(None, '(SINCE "{0}" BEFORE "{1}")'.format(
                date_from.strftime("%d-%b-%Y"),
                date_to.strftime("%d-%b-%Y")))
        else:
            type, data = self.__mail.search(None, '(SINCE "{0}")'.format(
                date_from.strftime("%d-%b-%Y")))

        for number_position in data[0].split():
            rv, data = self.__mail.fetch(number_position, self.__email_specification)

            message = email.message_from_bytes(data[0][1])
            hdr = email.header.make_header(email.header.decode_header(message['Subject']))

            new_letter = Letter()
            new_letter.id = int(number_position)
            new_letter.subject = str(hdr)
            new_letter.body = message.get_payload(i=0)
            new_letter.date = message['Date']

            for part in message.walk():
                if part.get_content_maintype() == 'multipart':
                    continue
                if part.get('Content-Disposition') is None:
                    continue

                attachment = Attachment()
                attachment.name = part.get_filename()
                attachment.text = part.get_payload(decode=True)
                attachment.stream = part.get_payload(decode=True)

                new_letter.attachments.append(attachment)

            letters.append(new_letter)

        return letters
