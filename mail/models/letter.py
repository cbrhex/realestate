from datetime import datetime


class Letter:
    def __init__(self):
        self.id = 0
        self.subject = ""
        self.body = ""
        self.date = datetime.today()
        self.attachments = []
