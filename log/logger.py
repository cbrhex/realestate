# -*- coding: utf-8 -*-
from datetime import datetime

from termcolor import colored


class Logger:

    @staticmethod
    def message(text: str):
        print(colored('[%s]: ' % datetime.now().strftime('%H:%M:%S'), 'blue') + colored(text, 'green'))

    @staticmethod
    def error(text: str):
        print(colored('[%s]: ' % datetime.now().strftime('%H:%M:%S'), 'blue') + colored(text, 'red'))

    @staticmethod
    def warning(text: str):
        print(colored('[%s]: ' % datetime.now().strftime('%H:%M:%S'), 'blue') + colored(text, 'yellow'))

    @staticmethod
    def split():
        print(colored('======================================================', 'cyan'))
