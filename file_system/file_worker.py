from file_system.drivers.driver import AbstractFileSystemDrive


class FileWorker:
    def __init__(self, driver: AbstractFileSystemDrive):
        self.driver = driver

    def get_file(self, name: str, type: str = None) -> str:
        content = self.driver.get_file_by_name(name, type)

        return content
