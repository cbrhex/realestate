import os
import json
from pathlib import Path

from var import ROOT_DIR


class SessionDump:
    __dump_path = '%s/session/dump.json' % ROOT_DIR

    def __init__(self):
        if not self.__is_dump_exits() or self.__is_dump_empty():
            self.__create_dump()

    def __create_dump(self):
        with open(self.__dump_path, 'w') as outfile:
            data = {'last_email_id': 0}

            json.dump(data, outfile)

    def __is_dump_exits(self) -> bool:
        my_file = Path(self.__dump_path)

        return my_file.is_file()

    def __is_dump_empty(self):
        return os.stat(self.__dump_path).st_size == 0

    def __read_file(self):
        with open(self.__dump_path) as file:
            data = json.load(file)

            return data

    def __write_file(self, data: {}):
        with open(self.__dump_path, 'w') as outfile:
            json.dump(data, outfile)

    def set(self, key: str, val):
        obj = self.__read_file()
        obj[key] = val

        self.__write_file(obj)

    def get(self, key):
        obj = self.__read_file()

        return obj[key]
