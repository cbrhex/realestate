from abc import ABC, abstractmethod

from file_system.models.file import File


class AbstractFileSystemDrive(ABC):

    def __init__(self):
        super().__init__()

    @abstractmethod
    def get_file_by_name(self, name: str, type: str = None) -> str:
        pass

    @abstractmethod
    def get_file_stream_by_id(self, id: str) -> str:
        pass

    @abstractmethod
    def find_file_info_by_name(self, name: str, type: str = None) -> [File]:
        pass
