import var
from file_system.drivers.driver import AbstractFileSystemDrive
from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools

from file_system.models.file import File


class GoogleDrive(AbstractFileSystemDrive):
    SCOPES = 'https://www.googleapis.com/auth/drive'

    def __init__(self):
        super().__init__()

        self.service = None

        self.__auth()

    def __auth(self):
        store = file.Storage('%s/session/google_drive_token.json' % var.ROOT_DIR)
        creds = store.get()

        if not creds or creds.invalid:
            flow = client.flow_from_clientsecrets('%s/session/google_drive_credentials.json' % var.ROOT_DIR,
                                                  self.SCOPES)
            creds = tools.run_flow(flow, store)

        self.service = build('drive', 'v3', http=creds.authorize(Http()))

    def get_file_by_name(self, name: str, type: str = None) -> File:
        files = self.find_file_info_by_name(name, type)

        if len(files) > 0:
            file_id = files[0].id
            file_name = files[0].name

            file_source = File()
            file_source.name = file_name
            file_source.id = file_id
            file_source.stream = self.get_file_stream_by_id(file_id)

            return file_source

    def get_file_stream_by_id(self, id: str) -> str:
        content = self.service \
            .files() \
            .get_media(fileId=id) \
            .execute()

        return content

    def find_file_info_by_name(self, name: str, type: str = None) -> [File]:
        page_token = None
        files = []

        query = "name contains '{0}'".format(name)

        if type is not None:
            query += ' and mimeType="{0}"'.format(type)

        while True:
            response = self.service.files().list(q=query,
                                                 spaces='drive',
                                                 fields='nextPageToken, files(id, name, mimeType)',
                                                 pageToken=page_token).execute()
            if response is not None:
                for file_item in response.get('files', []):
                    found_file_data = File()
                    found_file_data.id = file_item.get('id')
                    found_file_data.name = file_item.get('name')

                    files.append(found_file_data)

            page_token = response.get('nextPageToken', None)
            if page_token is None:
                break

        return files
