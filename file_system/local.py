class LocalFile:

    @staticmethod
    def red_file(path: str):
        with open(path, mode='r') as file:
            return file.read()

    @staticmethod
    def red_file_as_byte(path: str) -> bytes:
        with open(path, mode='rb') as file:
            stream = file.read()

        return stream
