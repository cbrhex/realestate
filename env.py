import configparser

import var

environment = configparser.ConfigParser()
environment.read('%s/session/config.ini' % var.ROOT_DIR)


def get(part, key, default=None):
    try:
        env = environment[part][key]
    except KeyError:
        return default

    return env
