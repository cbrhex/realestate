import re
import xml.etree.ElementTree

from mail.models.attachment import Attachment


class EmailData:
    id = 0
    form_of_address = ''
    first_name = ''
    last_name = ''
    email = ''
    url = ''


class EmailDataParser:
    def __init__(self, body: str, attachments: [Attachment]):
        self.body = body
        self.attachments = attachments

    @staticmethod
    def __parse(key: str, data: str) -> str:
        parse = re.search('(?<=%s:)\s[^\n]+' % key, data)

        if parse is None:
            return ''

        return parse.group(0).strip()

    def __xml_parse(self, key: str, data: str) -> str:
        root = xml.etree.ElementTree.fromstring(data)

        return root.find(key).text

    def get_data(self) -> EmailData:
        data = EmailData()
        data.form_of_address = self.__parse('Anrede', self.body)
        data.first_name = self.__parse('Vorname', self.body)
        data.last_name = self.__parse('Nachname', self.body)
        data.email = self.__parse('E-Mail', self.body)

        if len(self.attachments) > 0:
            data.id = self.__xml_parse('./objekt/oobj_id', self.attachments[0].text.decode("utf-8"))
            data.url = self.__xml_parse('./objekt/expose_url', self.attachments[0].text.decode("utf-8"))

        return data
