# -*- coding: utf-8 -*-
import signal
import sys
from datetime import datetime
import env
from file_system.local import LocalFile
from file_system.models.file import File
from file_system.session_dump import SessionDump
from log.logger import Logger
from mail.mails.to_client_mail import ToClientMail
from mail.mails.to_company_mail import ToCompanyMail
from mail.mails.to_task_mail import ToTaskMail
from mail.models.letter import Letter
from parsing.email_data_parser import EmailDataParser
from services.file_worker import FileWorkerService
from services.mail_reader import MailReaderService
from services.mail_sender import MailSenderService

import schedule
import time

from var import ROOT_DIR

exited = False


class Mailer:
    SCHEDULE_DELAY = int(env.get('MISC', 'SCHEDULE_DELAY', 300))
    TIMER_DELAY = 1  # sec
    DEBUG = env.get('MISC', 'DEBUG', False)
    DEBUG_LVL = int(env.get('MISC', 'DEBUG_LVL', 1))

    session = SessionDump()
    file_system = FileWorkerService.file()
    email_sender = MailSenderService.sender()

    def run(self):
        schedule.every(self.SCHEDULE_DELAY).seconds.do(self.execute)
        Logger.message('Set schedule to %ss' % self.SCHEDULE_DELAY)

        if not self.DEBUG:
            self.run_schedule()
        else:
            Logger.warning('Debug mode is enabled.')

            if self.DEBUG_LVL == 1:
                while 1:
                    res = input("Start again? (Y|n|schedule): ")

                    if res == 'n' or res == 'no' or res == 'No':
                        break
                    elif res == 'Y' or res == 'y' or res == 'yes':
                        self.execute()
                    elif res == 'schedule':
                        self.run_schedule()
                    else:
                        Logger.error('Command not found.')

            if self.DEBUG_LVL == 2:
                self.execute()

            if self.DEBUG_LVL == 3:
                self.run_schedule()

    def run_schedule(self):
        Logger.message('Run schedule')

        while 1:
            schedule.run_pending()
            time.sleep(self.TIMER_DELAY)

    def filter_new_emails(self, emails: [Letter]) -> [Letter]:
        new_emails = []
        if not emails:
            return []

        emails.sort(key=lambda x: x.id, reverse=False)
        bigger_id = emails[-1].id
        last_id = self.session.get('last_email_id')

        for email in emails:
            if email.id > last_id and email.attachments and 'contact_' in email.attachments[0].name:
                new_emails.append(email)

        self.session.set('last_email_id', bigger_id)

        return new_emails

    def execute(self):
        # get emails
        mr = MailReaderService.reader()

        Logger.message('Read all emails by date: %s' % datetime.now().strftime('%d %B %Y'))
        emails = mr.all_by_date(datetime.now())

        # check by id only new
        emails = self.filter_new_emails(emails)
        Logger.message('Emails found: %s' % len(emails))

        # send mails
        for mail in emails:
            Logger.message('Read email: %s' % mail.subject)

            # parse email data
            data = EmailDataParser(mail.body.as_string(), mail.attachments).get_data()

            # find files from google drive
            attachment_1 = self.file_system.get_file('expose-%s' % data.id, 'application/pdf')

            if not attachment_1:
                Logger.error('Error load attachment: %s' % ('expose-%s' % data.id))
                Logger.split()
                continue

            Logger.message('Found attachment from google drive: %s' % ('expose-%s' % data.id))

            # load info file
            attachment_2 = File()
            attachment_2.name = 'Widerrufsbelehrung für Verbraucher.pdf'
            attachment_2.stream = LocalFile().red_file_as_byte(
                '%s/storage/consumer_revocation_instruction.pdf' % ROOT_DIR)

            Logger.message('Load Widerrufsbelehrung file')

            # send to emails
            # to client
            Logger.message('Sending email to client: %s' % data.email)

            mail_to_client = ToClientMail(email=data.email,
                                          address=data.form_of_address,
                                          first_name=data.first_name,
                                          last_name=data.last_name,
                                          obj_id=data.id)

            mail_to_client.add_attachment(attachment_1)
            mail_to_client.add_attachment(attachment_2)
            self.email_sender.send(mail_to_client)

            Logger.message('Sent email to client: %s' % data.email)

            # to company
            mail_to_company = ToCompanyMail(email=data.email,
                                            address=data.form_of_address,
                                            first_name=data.first_name,
                                            last_name=data.last_name,
                                            obj_id=data.id,
                                            url=data.url)

            self.email_sender.send(mail_to_company)

            Logger.message('Sent email to company. Oobj id: %s' % data.id)

            # to task
            mail_to_task = ToTaskMail(email=data.email,
                                      address=data.form_of_address,
                                      first_name=data.first_name,
                                      last_name=data.last_name,
                                      obj_id=data.id,
                                      url=data.url)

            self.email_sender.send(mail_to_task)

            Logger.message('Sent email to task. Oobj id: %s' % data.id)
            Logger.split()

        Logger.split()


def signal_handler(sig, frame):
    global exited

    if not exited:
        exited = True
        Logger.split()
        Logger.warning('Application is closing...')

        time.sleep(2)
        Logger.warning('If you have some problems with application please contact: cyber@chaosbyte.io')
        sys.exit(0)


if __name__ == '__main__':
    Logger.message('Run application.')
    signal.signal(signal.SIGINT, signal_handler)

    mailer = Mailer()
    mailer.run()
