import env


class MailConfig:
    class Incoming:
        host = env.get("MAIL_INCOMING", "HOST", "imap.gmail.com")
        port = env.get("MAIL_INCOMING", "PORT", 995)
        login = env.get("MAIL_INCOMING", "LOGIN", "user@gmail.com")
        password = env.get("MAIL_INCOMING", "PASSWORD", "Password123")

    class Outgoing:
        host = env.get("MAIL_OUTGOING", "HOST", "smtp.gmail.com")
        port = env.get("MAIL_OUTGOING", "PORT", 587)
        login = env.get("MAIL_OUTGOING", "LOGIN", "company@gmail.com")
        password = env.get("MAIL_OUTGOING", "PASSWORD", "Password123")
        email_from = env.get("MAIL_OUTGOING", "FROM", "company@gmail.com")
