import env


class FileSystemConfig:
    class GoogleDrive:
        client_id = env.get("GOOGLE_DRIVE", "CLIENT_ID")
        client_secret = env.get("GOOGLE_DRIVE", "CLIENT_SECRET")
